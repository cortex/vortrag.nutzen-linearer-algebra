# 👨‍🏫 Nutzen Linearer Algebraa

Vortrag über den Nutzen und die Anwendbarkeit des Moduls Lineare Algebra.

[Keynotes: Lineare Algebra - Wozu das ganze?](https://hackmd.io/@cortex359/B1xRWKhwj)

## Usage

Enter python virtual environment on windows

```powershell
.\.venv\Scripts\activate.bat
```

or on linux 

```bash
source .venv/Scripts/activate
```

and install required packages with pip:

```bash
python -m pip install -r requirements.txt
cd src
python digit_recognizer.py
```

## Data Sets

### EMNIST (Extended MNIST)

Source: Kaggle [crawford/emnist](https://www.kaggle.com/datasets/crawford/emnist)

License: CC0: Public Domain

Files:
 - emnist-digits-train.csv (Digits 240,000 / MNIST 60,000)
 - emnist-digits-test.csv (Digits 40,000 / MNIST 10,000)

Cohen, G., Afshar, S., Tapson, J., & van Schaik, A. (2017). EMNIST: an extension of MNIST to handwritten letters.

Dataset retrieved from https://www.nist.gov/itl/iad/image-group/emnist-dataset

> The EMNIST dataset is a set of handwritten character digits derived from the NIST Special Database 19 and converted to a 28x28 pixel image format and dataset structure that directly matches the MNIST dataset. Further information on the dataset contents and conversion process can be found in the paper available at https://arxiv.org/abs/1702.05373v1.

> Digits and MNIST datsets
> 
> The EMNIST Digits and EMNIST MNIST dataset provide balanced handwritten digit datasets directly compatible with the original MNIST dataset. 

### Digit Recognizer (MNIST data)

Source: Kaggle [competitions/digit-recognizer](https://www.kaggle.com/competitions/digit-recognizer/data)

License: Creative Commons Attribution-Share Alike 3.0 license.

Files:
 - train.csv
 - test.csv 