![image](imgs/logo-lfi.png)

# XeLaTeX Präsentationsvorlage

## Hinweise zur Verwendung

Es muss XeLaTeX verwendet werden und die Micosoft Schriftart Arial installiert sein, um dem Corporat Design zu genügen (siehe Alltagsschrift).

Als Corporate Font/Hausschrift gilt *Helvetica Neue*, für die eine Lizenz benötigt wird, welche über das Softwareportal des IT Centers bezogen werden kann.

## Quellen

Basierend auf der Präsentationsvorlage [1411_Präsentation_LaTeX](https://www9.rwth-aachen.de/global/show_mimetype.asp?id=%2Ezip) in der Version 2015/05 aus dem [Vorlagencenter Institute](https://www9.rwth-aachen.de/go/id/bnnp/dir/aaaaaaaaaaabnnw/).


Richtlinien aus dem Manual [^1] sowie [^2] und [^3]

Gestaltungsrichtlinien zum Corporate Design für die Institute, Lehrstühle und zentrale Einrichtungen [Interne Seite](https://www9.rwth-aachen.de/go/id/bnxz)

[^1]: Corporate Design. RWTH Aachen. Grundlagen. 08.2014 https://www9.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaadrbpl

[^2]: Corporate Design. RWTH Aachen. Institute und Einrichtungen.Präsentationsmedien. 08.2014 https://www9.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaadrbph


[^3]: Corporate Design. RWTH Aachen. Institute und Einrichtungen. Grafiken und Bildsprache. 08.2014 https://www9.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaadrbpi

## Weitere Quellen

[RWTH-Imagebilder](https://imagebilder.rwth-aachen.de/search) und Piktogramme


[Trillu Online-Generator](https://www.illustration.rwth-aachen.de/php/login.php), Passwort `trillu_rwth`


# ToDo:
Creative Commons Logo hinzufügen